import { createConnection } from "typeorm";

export const connection = createConnection({
  type: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "QWErty1234",
  database: "collection_cards",
  synchronize: true,
  logging: true,
  entities: [__dirname + "/*.js"],
});
