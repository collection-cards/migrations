import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

enum Status {
  ALIVE = "alive",
  DEAD = "dead",
  UNKNOWN = "unknown",
}

enum Gender {
  FEMALE = "female",
  MALE = "male",
  GENDERLESS = "genderless",
  UNKNOWN = "unknown",
}

@Entity({ name: "cards" })
export class Card {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ name: "name" })
  name: string;

  @Column({
    name: "status",
    type: "enum",
    enum: Status,
    default: Status.UNKNOWN,
  })
  status: Status;

  @Column({
    name: "gender",
    type: "enum",
    enum: Gender,
    default: Gender.UNKNOWN,
  })
  gender: Gender;

  @Column({ name: "image" })
  image: string;
}
