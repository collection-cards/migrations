import { getRepository } from "typeorm";
import { Card } from "../entity/Card";

export const getCardList = async () => {
  const cardList = await getRepository(Card)
    .createQueryBuilder("cards")
    .getMany();
  return cardList;
};

export const getCardById = async (id: string) => {
  const card = await getRepository(Card)
    .createQueryBuilder("cards")
    .where("card.id = :id", { id })
    .getOne();
  return card;
};

export const insertOne = async () => {
  console.log("insertOne");
};

export const insertMany = async () => {
  console.log("insertMany");
};
