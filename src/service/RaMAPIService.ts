import axios from "axios";
import { getRepository } from "typeorm";
import { Card } from "../entity/Card";

const RaMAPIUrl = "https://rickandmortyapi.com/api/character";

export const deleteCollection = async () => {
  try {
    await getRepository(Card)
      .createQueryBuilder("cards")
      .delete()
      .from(Card)
      .execute();
  } catch (error) {
    console.log(error);
  }
};

const getCards = async (url: string, result: any[] = []) => {
  try {
    const res = await axios.get(url);
    if (res.data) {
      const items = res.data.results.map((item: any) => {
        const { name, image, gender, status } = item;
        return { name, image, gender, status };
      });
      result.push(...items);
    }
    if (res.data.info.next) {
      result = [...(await getCards(res.data.info.next, result))];
    }
    return result;
  } catch (error) {
    console.log(error);
  }
};

export const fillCollection = async () => {
  try {
    const items = await getCards(RaMAPIUrl);
    await getRepository(Card)
      .createQueryBuilder("cards")
      .insert()
      .into(Card)
      .values(items)
      .execute();
    process.exit(0);
  } catch (error) {
    console.log(error);
  }
};
