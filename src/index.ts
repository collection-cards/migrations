import { connection } from "./entity/connection";
import { insertOne } from "./service/CardService";
import { fillCollection, deleteCollection } from "./service/RaMAPIService";

const run = async () => {
  await connection;
  await deleteCollection();
  await fillCollection();
  // insertOne();
};

run();
