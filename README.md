# Migrations from [Rick and Morty API](https://rickandmortyapi.com/)

## Installation

Requires:  
[Node.js](https://nodejs.org/)  
[mysql-server](https://www.mysql.com/downloads/)  
[TypeScript](https://www.npmjs.com/package/typescript)

Clone repository and install the dependencies and devDependencies

```sh
cd migrations
npm i
tsc
node dist/index.js
```
